from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.primitives import serialization
from cryptography.fernet import Fernet
from .Hashes import SHA512


def gen_symmetric_key():
    return Fernet.generate_key()


def load_symmetric_key(key_file):
    with open(key_file, 'rb') as file:
        return file.read()


def encrypt_message_with_sym_key(sym_key, message):
    f = Fernet(sym_key)
    return f.encrypt(message)


def decrypt_message_with_sym_key(sym_key, message):
    f = Fernet(sym_key)
    return f.decrypt(message)


def gen_session_key_package(public_key):
    key = gen_symmetric_key()
    return encrypt_message(public_key, key, SHA512())


def gen_session_key_package_with_message(public_key, message):
    key = gen_symmetric_key()
    f = Fernet(key)
    encrypted_message = f.encrypt(message)
    encrypted_key = encrypt_message(public_key, key, SHA512())
    return encrypted_key, encrypted_message


def prepare_session_key_package_with_message(public_key, sym_key, message):
    f = Fernet(sym_key)
    encrypted_message = f.encrypt(message)
    encrypted_key = encrypt_message(public_key, sym_key, SHA512())
    return encrypted_key, encrypted_message


def gen_private_key(size):
    return rsa.generate_private_key(65537, size)


def gen_public_key(private_key):
    return private_key.public_key()


def load_private_key(private_key, password=None):
    with open(private_key, 'rb') as key_file:
        return serialization.load_pem_private_key(
            key_file.read(),
            password
        )


def load_public_key(public_key):
    with open(public_key, 'rb') as key_file:
        return serialization.load_pem_public_key(
            key_file.read()
        )


def serialize_private_key(private_key, format, encryption):
    return private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=format,
        encryption_algorithm=encryption
    )


def serialize_public_key(public_key):
    return public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )


def encrypt_message(public_key, message, algorithm):
    return public_key.encrypt(
        message,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=algorithm),
            algorithm=algorithm,
            label=None
        )
    )


def decrypt_message(private_key, message, algorithm):
    return private_key.decrypt(
        message,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=algorithm),
            algorithm=algorithm,
            label=None
        )
    )


No_encryption=serialization.NoEncryption

Best_encryption=serialization.BestAvailableEncryption
