import cryptography.hazmat.primitives.hashes as hs

SHA512=hs.SHA512
SHA256=hs.SHA256
SHA224=hs.SHA224
SHA384=hs.SHA384