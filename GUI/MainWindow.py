from PyQt5.QtWidgets import QMainWindow, QVBoxLayout, QHBoxLayout, QTabWidget, QPushButton, QLineEdit, QGroupBox, QWidget, QGridLayout, QLabel, QComboBox, QTextEdit


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        self.setWindowTitle('CryptoPy GUI')
        self.setCentralWidget(self.create_center_widget())

    def create_key_pair_widget(self) -> QWidget:
        widget = QWidget(self)
        layout = QHBoxLayout()

        # left box
        private_key_box = QGroupBox('Private key', self)
        priv_kb_layout = QGridLayout()

        priv_key_path_edit = QLineEdit(self)
        priv_key_path_btn = QPushButton('Choose file', self)
        priv_key_path_lbl = QLabel('Output file path', self)

        priv_key_length_lbl = QLabel('Length', self)
        priv_key_length_box = QComboBox(self)
        priv_key_length_box.addItem('2048', 2048)
        priv_key_length_box.addItem('4096', 4096)

        priv_key_output_group = QGroupBox('Result', self)
        priv_key_output_group_layout = QVBoxLayout()
        priv_key_output_text_edit = QTextEdit(self)
        priv_key_output_text_edit.setEnabled(False)
        priv_key_output_text_edit.setReadOnly(True)
        priv_key_output_group_layout.addWidget(priv_key_output_text_edit)
        priv_key_output_group.setLayout(priv_key_output_group_layout)

        priv_key_gen_btn = QPushButton('Generate key', self)

        priv_kb_layout.addWidget(priv_key_path_lbl, 0, 0)
        priv_kb_layout.addWidget(priv_key_path_edit, 0, 1)
        priv_kb_layout.addWidget(priv_key_path_btn, 0, 2)

        priv_kb_layout.addWidget(priv_key_length_lbl, 1, 0)
        priv_kb_layout.addWidget(priv_key_length_box, 1, 1, 1, 2)

        priv_kb_layout.addWidget(priv_key_output_group, 2, 0, 1, 3)

        priv_kb_layout.addWidget(priv_key_gen_btn, 3, 0, 1, 3)

        private_key_box.setLayout(priv_kb_layout)

        # right box
        public_key_box = QGroupBox('Public key', self)
        pub_kb_layout = QGridLayout()

        pub_priv_key_path_edit = QLineEdit(self)
        pub_priv_key_path_btn = QPushButton('Choose file', self)
        pub_priv_key_path_lbl = QLabel('Private key', self)

        pub_pub_key_path_edit = QLineEdit(self)
        pub_pub_key_path_btn = QPushButton('Choose file', self)
        pub_pub_key_path_lbl = QLabel('Public key', self)

        pub_key_output_group = QGroupBox('Result', self)
        pub_key_output_group_layout = QVBoxLayout()
        pub_key_output_text_edit = QTextEdit(self)
        pub_key_output_text_edit.setEnabled(False)
        pub_key_output_text_edit.setReadOnly(True)
        pub_key_output_group_layout.addWidget(pub_key_output_text_edit)
        pub_key_output_group.setLayout(pub_key_output_group_layout)

        pub_key_gen_btn = QPushButton('Generate key', self)

        pub_kb_layout.addWidget(pub_priv_key_path_lbl, 0, 0)
        pub_kb_layout.addWidget(pub_priv_key_path_edit, 0, 1)
        pub_kb_layout.addWidget(pub_pub_key_path_btn, 0, 2)

        pub_kb_layout.addWidget(pub_pub_key_path_lbl, 1, 0)
        pub_kb_layout.addWidget(pub_pub_key_path_edit, 1, 1)
        pub_kb_layout.addWidget(pub_priv_key_path_btn, 1, 2)

        pub_kb_layout.addWidget(pub_key_output_group, 2, 0, 1, 3)

        pub_kb_layout.addWidget(pub_key_gen_btn, 3, 0, 1, 3)

        public_key_box.setLayout(pub_kb_layout)

        layout.addWidget(private_key_box)
        layout.addWidget(public_key_box)
        widget.setLayout(layout)
        return widget

    def create_sym_encryption_widget(self):
        widget = QWidget(self)
        prime_layout = QVBoxLayout()
        layout = QHBoxLayout()

        # left widget
        priv_decrypt_box = QGroupBox('Symmetric key decryption', self)
        priv_decrypt_layout = QGridLayout()

        priv_decrypt_msg_lbl = QLabel('Load encrypted file', self)
        priv_decrypt_msg_edit = QLineEdit(self)
        priv_decrypt_msg_btn = QPushButton('Choose file', self)

        priv_decrypt_btn = QPushButton('Decrypt', self)

        priv_decrypt_decrypted_box = QGroupBox('Decryption result', self)
        priv_decrypt_decrypted_layout = QVBoxLayout()
        priv_decrypt_decrypted_text_edit = QTextEdit(self)
        priv_decrypt_decrypted_text_edit.setEnabled(False)
        priv_decrypt_decrypted_text_edit.setReadOnly(True)

        priv_decrypt_decrypted_btn_layout = QHBoxLayout()
        priv_decrypt_decrypted_clipboard_btn = QPushButton('Copy to clipboard', self)
        priv_decrypt_decrypted_save_btn = QPushButton('Save to file', self)
        priv_decrypt_decrypted_btn_layout.addWidget(priv_decrypt_decrypted_clipboard_btn)
        priv_decrypt_decrypted_btn_layout.addWidget(priv_decrypt_decrypted_save_btn)

        priv_decrypt_decrypted_layout.addWidget(priv_decrypt_decrypted_text_edit)
        priv_decrypt_decrypted_layout.addLayout(priv_decrypt_decrypted_btn_layout)
        priv_decrypt_decrypted_box.setLayout(priv_decrypt_decrypted_layout)

        priv_decrypt_layout.addWidget(priv_decrypt_msg_lbl, 0, 0)
        priv_decrypt_layout.addWidget(priv_decrypt_msg_edit, 0, 1)
        priv_decrypt_layout.addWidget(priv_decrypt_msg_btn, 0, 2)

        priv_decrypt_layout.addWidget(priv_decrypt_btn, 1, 0, 1, 3)

        priv_decrypt_layout.addWidget(priv_decrypt_decrypted_box, 2, 0, 1, 3)
        priv_decrypt_box.setLayout(priv_decrypt_layout)

        # right widget
        pub_encrypt_box = QGroupBox('Symmetric key encryption', self)
        pub_encrypt_layout = QGridLayout()

        pub_encrypt_msg_lbl = QLabel('Load file', self)
        pub_encrypt_msg_edit = QLineEdit(self)
        pub_encrypt_msg_btn = QPushButton('Choose file', self)

        pub_encrypt_btn = QPushButton('Encrypt', self)

        pub_encrypt_encrypted_box = QGroupBox('Encryption result', self)
        pub_encrypt_encrypted_layout = QVBoxLayout()
        pub_encrypt_encrypted_text_edit = QTextEdit(self)
        pub_encrypt_encrypted_text_edit.setEnabled(False)
        pub_encrypt_encrypted_text_edit.setReadOnly(True)

        pub_encrypt_encrypted_btn_layout = QHBoxLayout()
        pub_encrypt_encrypted_clipboard_btn = QPushButton('Copy to clipboard', self)
        pub_encrypt_encrypted_save_btn = QPushButton('Save to file', self)
        pub_encrypt_encrypted_btn_layout.addWidget(pub_encrypt_encrypted_clipboard_btn)
        pub_encrypt_encrypted_btn_layout.addWidget(pub_encrypt_encrypted_save_btn)

        pub_encrypt_encrypted_layout.addWidget(pub_encrypt_encrypted_text_edit)
        pub_encrypt_encrypted_layout.addLayout(pub_encrypt_encrypted_btn_layout)
        pub_encrypt_encrypted_box.setLayout(pub_encrypt_encrypted_layout)

        pub_encrypt_layout.addWidget(pub_encrypt_msg_lbl, 0, 0)
        pub_encrypt_layout.addWidget(pub_encrypt_msg_edit, 0, 1)
        pub_encrypt_layout.addWidget(pub_encrypt_msg_btn, 0, 2)

        pub_encrypt_layout.addWidget(pub_encrypt_btn, 1, 0, 1, 3)

        pub_encrypt_layout.addWidget(pub_encrypt_encrypted_box, 2, 0, 1, 3)
        pub_encrypt_box.setLayout(pub_encrypt_layout)

        key_layout = QHBoxLayout()
        key_lbl = QLabel('Load sym key', self)
        key_edit = QLineEdit(self)
        key_btn = QPushButton('Choose file', self)
        key_layout.addWidget(key_lbl)
        key_layout.addWidget(key_edit)
        key_layout.addWidget(key_btn)

        layout.addWidget(priv_decrypt_box)
        layout.addWidget(pub_encrypt_box)

        prime_layout.addLayout(key_layout)
        prime_layout.addLayout(layout)

        widget.setLayout(prime_layout)
        return widget

    def create_sym_key_widget(self):
        return QWidget(self)

    def create_rsa_encryption_widget(self):
        widget = QWidget(self)
        layout = QHBoxLayout()

        # left widget
        priv_decrypt_box = QGroupBox('Private key decryption', self)
        priv_decrypt_layout = QGridLayout()

        priv_decrypt_priv_lbl = QLabel('Load private key', self)
        priv_decrypt_priv_edit = QLineEdit(self)
        priv_decrypt_priv_btn = QPushButton('Choose file', self)

        priv_decrypt_msg_lbl = QLabel('Load encrypted file', self)
        priv_decrypt_msg_edit = QLineEdit(self)
        priv_decrypt_msg_btn = QPushButton('Choose file', self)

        priv_decrypt_btn = QPushButton('Decrypt', self)

        priv_decrypt_decrypted_box = QGroupBox('Decryption result', self)
        priv_decrypt_decrypted_layout = QVBoxLayout()
        priv_decrypt_decrypted_text_edit = QTextEdit(self)
        priv_decrypt_decrypted_text_edit.setEnabled(False)
        priv_decrypt_decrypted_text_edit.setReadOnly(True)

        priv_decrypt_decrypted_btn_layout = QHBoxLayout()
        priv_decrypt_decrypted_clipboard_btn = QPushButton('Copy to clipboard', self)
        priv_decrypt_decrypted_save_btn = QPushButton('Save to file', self)
        priv_decrypt_decrypted_btn_layout.addWidget(priv_decrypt_decrypted_clipboard_btn)
        priv_decrypt_decrypted_btn_layout.addWidget(priv_decrypt_decrypted_save_btn)

        priv_decrypt_decrypted_layout.addWidget(priv_decrypt_decrypted_text_edit)
        priv_decrypt_decrypted_layout.addLayout(priv_decrypt_decrypted_btn_layout)
        priv_decrypt_decrypted_box.setLayout(priv_decrypt_decrypted_layout)

        priv_decrypt_layout.addWidget(priv_decrypt_priv_lbl, 0, 0)
        priv_decrypt_layout.addWidget(priv_decrypt_priv_edit, 0, 1)
        priv_decrypt_layout.addWidget(priv_decrypt_priv_btn, 0, 2)

        priv_decrypt_layout.addWidget(priv_decrypt_msg_lbl, 1, 0)
        priv_decrypt_layout.addWidget(priv_decrypt_msg_edit, 1, 1)
        priv_decrypt_layout.addWidget(priv_decrypt_msg_btn, 1, 2)

        priv_decrypt_layout.addWidget(priv_decrypt_btn, 2, 0, 1, 3)

        priv_decrypt_layout.addWidget(priv_decrypt_decrypted_box, 3, 0, 1, 3)
        priv_decrypt_box.setLayout(priv_decrypt_layout)

        # right widget
        pub_encrypt_box = QGroupBox('Public key encryption', self)
        pub_encrypt_layout = QGridLayout()

        pub_encrypt_pub_lbl = QLabel('Load public key', self)
        pub_encrypt_pub_edit = QLineEdit(self)
        pub_encrypt_pub_btn = QPushButton('Choose file', self)

        pub_encrypt_msg_lbl = QLabel('Load file', self)
        pub_encrypt_msg_edit = QLineEdit(self)
        pub_encrypt_msg_btn = QPushButton('Choose file', self)

        pub_encrypt_btn = QPushButton('Encrypt', self)

        pub_encrypt_encrypted_box = QGroupBox('Encryption result', self)
        pub_encrypt_encrypted_layout = QVBoxLayout()
        pub_encrypt_encrypted_text_edit = QTextEdit(self)
        pub_encrypt_encrypted_text_edit.setEnabled(False)
        pub_encrypt_encrypted_text_edit.setReadOnly(True)

        pub_encrypt_encrypted_btn_layout = QHBoxLayout()
        pub_encrypt_encrypted_clipboard_btn = QPushButton('Copy to clipboard', self)
        pub_encrypt_encrypted_save_btn = QPushButton('Save to file', self)
        pub_encrypt_encrypted_btn_layout.addWidget(pub_encrypt_encrypted_clipboard_btn)
        pub_encrypt_encrypted_btn_layout.addWidget(pub_encrypt_encrypted_save_btn)

        pub_encrypt_encrypted_layout.addWidget(pub_encrypt_encrypted_text_edit)
        pub_encrypt_encrypted_layout.addLayout(pub_encrypt_encrypted_btn_layout)
        pub_encrypt_encrypted_box.setLayout(pub_encrypt_encrypted_layout)

        pub_encrypt_layout.addWidget(pub_encrypt_pub_lbl, 0, 0)
        pub_encrypt_layout.addWidget(pub_encrypt_pub_edit, 0, 1)
        pub_encrypt_layout.addWidget(pub_encrypt_pub_btn, 0, 2)

        pub_encrypt_layout.addWidget(pub_encrypt_msg_lbl, 1, 0)
        pub_encrypt_layout.addWidget(pub_encrypt_msg_edit, 1, 1)
        pub_encrypt_layout.addWidget(pub_encrypt_msg_btn, 1, 2)

        pub_encrypt_layout.addWidget(pub_encrypt_btn, 2, 0, 1, 3)

        pub_encrypt_layout.addWidget(pub_encrypt_encrypted_box, 3, 0, 1, 3)
        pub_encrypt_box.setLayout(pub_encrypt_layout)

        layout.addWidget(priv_decrypt_box)
        layout.addWidget(pub_encrypt_box)
        widget.setLayout(layout)
        return widget

    def create_center_widget(self) -> QWidget:
        widget = QWidget(self)
        prime_layout = QVBoxLayout()

        tab_widget = QTabWidget(self)
        rsa_keys_widget = self.create_key_pair_widget()
        rsa_enc_widget = self.create_rsa_encryption_widget()
        sym_key_widget = self.create_sym_key_widget()
        sym_enc_widget = self.create_sym_encryption_widget()

        tab_widget.addTab(rsa_keys_widget, 'RSA keys')
        tab_widget.addTab(rsa_enc_widget, 'RSA encryption')
        tab_widget.addTab(sym_key_widget, 'Sym key')
        tab_widget.addTab(sym_enc_widget, 'Sym encryption')

        prime_layout.addWidget(tab_widget)
        widget.setLayout(prime_layout)
        return widget
