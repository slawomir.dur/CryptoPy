from Crypto.Operations import decrypt_message, encrypt_message, gen_private_key, gen_public_key, serialize_public_key, serialize_private_key, load_public_key, load_private_key, No_encryption, Best_encryption, gen_symmetric_key, encrypt_message_with_sym_key, decrypt_message_with_sym_key
from Crypto.Hashes import SHA512
from Crypto.Formats import PCKS8_PRIVATE
from GUI import Main
import cmd
import sys


class CmdProcessor(cmd.Cmd):
    prompt='(CryptoPy) $ '
    intro='Welcome to CryptoPy shell.\nType "help" or "?" to see the list of commands.\nContact the developer via: slawomir.dur@gmail.com\nIf you want to see more: gitlab.com/slawomir.dur\n\nNote: To run with graphical interface, please execute the programme with "gui" command'
    doc_header="The list of all commands that CryptoPy supports\nTo learn more type 'help <command>'"

    def do_gen_priv_key(self, arg):
        'Generates private key. Exemplary call: gen_priv_key length output_file password (optional)'
        args=arg.strip().split()
        length=int(args[0])
        file=args[1] if len(args) > 1 else None
        password=args[2] if len(args) > 2 else None
        print(f"Generating private key of length {length} in {file} with pwd {password}")
        priv = gen_private_key(int(length))
        encryption = No_encryption()
        if password is not None:
            encryption = Best_encryption(password)
        serialized = serialize_private_key(priv, PCKS8_PRIVATE, encryption)
        if file is not None:
            with open(file, 'wb+') as file:
                file.write(serialized)

    def do_gen_pub_key(self, arg):
        'Generates public key. Exemplary call: gen_pub_key private_key output_file password (optional)'
        args=arg.strip().split()
        private_key_file=args[0]
        output_file=args[1]
        pwd=args[2] if len(args) > 2 else None

        private_key=load_private_key(private_key_file,pwd) if pwd is not None else load_private_key(private_key_file)
        public_key = gen_public_key(private_key)
        serialized = serialize_public_key(public_key)
        with open(output_file, 'wb+') as file:
            file.write(serialized)

    def do_encrypt_with_pub_key(self, arg):
        'Encrypts file with a message.\nOutput file always lies in the directory of message_file and has a name equal to that of message_file but with a suffix ".enc".\nExemplary call: encrypt message_file recipients_public_key'
        args=arg.strip().split()
        message_file=args[0]
        public_key_file=args[1]
        public_key=load_public_key(public_key_file)
        with open(message_file, 'rb') as file:
            msg_bytes = file.read()
        encrypted = encrypt_message(public_key, msg_bytes, SHA512())
        message_file += '.enc'
        with open(message_file, 'wb+') as file:
            file.write(encrypted)

    def do_decrypt_with_priv_key(self, arg):
        'Decrypts message.\nOutput file always lies in the directory of message_file and has a name equal to that of message_file but with a suffix ".dec".\nExemplary call: decrypt message_file private_key password (optional)'
        args=arg.strip().split()
        message_file=args[0]
        private_key=args[1]
        pwd=args[2] if len(args) > 2 else None
        priv_key=load_private_key(private_key, pwd)
        with open(message_file, 'rb') as file:
            message=file.read()
        decrypted=decrypt_message(priv_key, message, SHA512())
        with open(message_file+".dec", 'wb+') as file:
            file.write(decrypted)

    def do_gen_sym_key(self, arg):
        'Generates Fernet 128-bit symmetric key with AES'
        args = arg.strip().split()
        key_file = args[0]
        key = gen_symmetric_key()
        with open(key_file, 'wb+') as file:
            file.write(key)


    def do_encrypt_with_sym_key(self, arg):
        'Loads sym key and encrypts a message file with it.\nThe resultant file has the same name as the message file but with ".enc" extension.'
        args = arg.strip().split()
        key_file = args[0]
        message_file = args[1]
        with open(key_file, 'rb') as file:
            key = file.read()
        with open(message_file, 'rb') as file:
            message = file.read()
        encrypted_message = encrypt_message_with_sym_key(key, message)
        with open(message_file+'.enc', 'wb+') as file:
            file.write(encrypted_message)

    def do_decrypt_with_sym_key(self, arg):
        'Loads sym key and decrypts a message file with it.\nThe resultant file has the same name as the message file but with ".dec" extension.'
        args = arg.strip().split()
        key_file = args[0]
        message_file = args[1]
        with open(key_file, 'rb') as file:
            key = file.read()
        with open(message_file, 'rb') as file:
            message = file.read()
        decrypted_message = decrypt_message_with_sym_key(key, message)
        with open(message_file + '.dec', 'wb+') as file:
            file.write(decrypted_message)


    def do_EOF(self, line):
        return True

    def do_exit(self, arg):
        'Exits the programme'
        print('Thank you for using CryptoPy')
        return True


if __name__ == '__main__':
    if len(sys.argv) == 1:
        CmdProcessor().cmdloop()
    elif len(sys.argv) == 2 and sys.argv[1].lower() == 'gui':
        Main.main(sys.argv)
