# Easy encryption project

This is to allow easy creation of both RSA keys and symmetric keys. 

It supports both GUI and command line.

By default it runs in cmd.

To run GUI you need to provide 'gui' cmd argument.

# How to use

| Command | Parameters |
| ------ | ------ |
| gen_priv_key | length output_file password (optional) |
| gen_pub_key | private_key output_file |
| encrypt | message_file public_key |
| decrypt | messgae_file private_key password (optional) |
